<?php

namespace App\Http\Controllers;
use App\Models\Posts;
use Illuminate\Http\Request;
use Auth;
use Session;

class PostsController extends Controller
{
    public function __construct() {
        // parent::__construct();
    }

    public function getPosts(Request $request) {
        $posts = new Posts();
        $paginate = 5 * $request->page;
        
        // return response()->json($posts->orderBy('created_at', 'desc')->paginate(5));
        return response()->json(['data'=>$posts->orderBy('created_at', 'desc')->skip(0)->take($paginate)->get()]);
    }

    public function addPosts(Request $request) {
        $posts = new Posts();
        
        $posts->image =  $request->image;
        $posts->title =  $request->title;
        $posts->content =  $request->content;
        $posts->save();

        return response()->json(['token' => true]);
        
    }
}
