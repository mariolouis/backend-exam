<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use Validator;
use Session;

class UsersController extends Controller
{
    public function __construct() {
        // parent::__construct();
    }

    public function login(Request $request) {
        
        $validator = $this->validate($request, [
                'email' => 'required|email|exists:App\Models\Users,email',
                'password' => 'required|min:4'
            ], [
                'email.required' => 'The email is required.',
                'email.email' => 'The email needs to have a valid format.',
                'email.exists' => 'The email doesn\'t exists.',
                'password.required' => 'The password is required',
                'password.min' => 'The password needs to have a minimum of 4 characters.',
            ]
        );
        
        
        if ( Auth::attempt( [ 'email' => $request->email, 'password' => $request->password ] ) ){
            return response()->json(Auth::user());
        }
    }

    public function logout() {
        Auth::logout();
        return redirect();
    }

    public function register(Request $request) {
        $validator = $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:App\Models\Users,email',
                'password' => 'required|min:4',
                'password_confirmation' => 'required|same:password'
            ], [
                'name.required' => 'The name is required.',
                'email.required' => 'The email is required.',
                'email.email' => 'The email needs to have a valid format.',
                'email.exists' => 'The email doesn\'t exists.',
                'password.required' => 'The password is required',
                'password_confirmation.required' => 'The confirm password is required',
                'password_confirmation.same' => 'The confirm password is doesn\'t match your password',
            ]
        );

        $user = new Users();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->token = Str::random(10);
        $user->save();

        return response()->json([ 'id' => $user->id ]);   

    }
}
