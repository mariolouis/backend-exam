<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Users;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::create(['name' => 'admin',
                        'email' => 'admin@admin.com',
                        'password' => Hash::make('admin'),
                        'token' => Str::random(10)
                    ]);
    }
}
