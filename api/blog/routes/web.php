<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('api/login', 'UsersController@login');
Route::post('api/logout', 'UsersController@logout');
Route::post('api/register', 'UsersController@register');
Route::get('api/posts/{page?}', 'PostsController@getPosts');
Route::post('api/posts', 'PostsController@addPosts');
