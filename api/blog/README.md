Set Up:

-   move to api project using `cd api/blog`
-   run `php artisan serve --port="8000"` in api project
-   run `php artisan migrate` to upload database
-   run `composer dump-autoload` and `php artisan db:seed` for ready made user
